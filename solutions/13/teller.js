// node teller.js register
// -> get pub/priv key here
// node teller.js deposit pub priv 25

var jsonStream = require("duplex-json-stream");
var net = require("net");
const EthCrypto = require("eth-crypto");

const keythereum = require("keythereum");

var CryptoJS = require("crypto-js");
var EC = require("elliptic").ec;
var ec = new EC("secp256k1");
const web3u = require("web3-utils");

var client = jsonStream(net.connect(3876));
var argv = process.argv.slice(2);

var command = argv[0];
console.log(command);

client.on("data", function(msg) {
  console.log("Teller received:", msg);
});

switch (command) {
  case "register":
    keyPair = {};
    private = computePrivKey();
    public = computeAddressFromPrivKey(private);
    keyPair.private = "0x" + private;
    keyPair.public = "0x" + public;
    tx = { cmd: "register", keyPair: keyPair };
    console.log(`created public/private ${keyPair.public} ${keyPair.private}`);
    client.end(tx);
    break;

  case "balance":
    var public = argv[1];
    var private = argv[2];

    var tx = { cmd: "balance" };
    tx.keyPair = { public: public, private: private };

    var message = createSignedWithKeys(tx);

    client.end(tx);
    break;

  case "deposit":
    var public = argv[1];
    var private = argv[2];

    var amount = parseFloat(argv[3]);
    var tx = { cmd: "deposit", amount: amount };
    tx.keyPair = { public: public, private: private };

    var message = createSignedWithKeys(tx);

    client.end(message);
    break;

  case "withdraw":
    var public = argv[1];
    var private = argv[2];

    var amount = parseFloat(argv[3]);
    tx = { cmd: "withdraw", amount: amount };
    tx.keyPair = { public: public, private: private };

    message = createSignedWithKeys(tx);
    client.end(message);
    break;

  case "help":
  default:
    console.log("node teller.js public private [CMD]");
}

function createSignedWithKeys(tx) {
  return {
    tx: tx,
    signature: EthCrypto.sign(
      tx.keyPair.private,
      web3u.sha3(JSON.stringify(tx))
    )
  };
}

function computeAddressFromPrivKey(privKey) {
  var keyPair = ec.genKeyPair();
  keyPair._importPrivate(privKey, "hex");
  var compact = false;
  var pubKey = keyPair.getPublic(compact, "hex").slice(2);
  var pubKeyWordArray = CryptoJS.enc.Hex.parse(pubKey);
  var hash = CryptoJS.SHA3(pubKeyWordArray, { outputLength: 256 });
  var address = hash.toString(CryptoJS.enc.Hex).slice(24);

  return address;
}

function computePrivKey() {
  var params = { keyBytes: 32, ivBytes: 16 };

  var dk = keythereum.create(params);
  //console.log(dk);

  let private = dk.privateKey.toString("hex");
  return private;
}
