var jsonStream = require("duplex-json-stream");
var net = require("net");

var client = jsonStream(net.connect(3876));
var argv = process.argv.slice(2);

var command = argv[0];
console.log(command);

client.on("data", function(msg) {
  console.log("Teller received:", msg);
});

switch (command) {
  case "register":
    var cusomerId = parseFloat(argv[1]);
    client.end({ cmd: "register", customerId: cusomerId });
    break;

  case "deposit":
    var cusomerId = parseFloat(argv[1]);
    var amount = parseFloat(argv[2]);

    client.end({ cmd: "deposit", amount: amount, customerId: cusomerId });
    break;

  case "balance":
    var cusomerId = parseFloat(argv[1]);
    client.end({ cmd: "balance", customerId: cusomerId });
    break;

  case "withdraw":
    var cusomerId = parseFloat(argv[1]);
    var amount = parseFloat(argv[2]);
    client.end({ cmd: "withdraw", amount: amount, customerId: cusomerId });
    break;

  case "help":
  default:
    console.log("node teller.js cusomerId [CMD]");
}
