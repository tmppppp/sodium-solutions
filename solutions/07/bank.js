var jsonStream = require("duplex-json-stream");
var net = require("net");
const fs = require("fs");
const { sha256 } = require("ethereumjs-util");
const crypto = require("crypto");

const web3u = require("web3-utils");

var log = [];
fs.writeFile("./log.json", "[]", { flag: "wx" }, function(err) {});

var server = net.createServer(function(socket) {
  socket = jsonStream(socket);

  socket.on("data", function(msg) {
    console.log("Bank received:", msg);
    log = JSON.parse(fs.readFileSync("./log.json", "utf8"));
    verifyChainIntegrity(log);
    switch (msg.cmd) {
      case "balance":
        socket.end({ cmd: "balance", balance: log.reduce(reduceLog, 0) });
        break;
      case "deposit":
        appendToTransactionLog(msg);
        socket.end({ cmd: "balance", balance: log.reduce(reduceLog, 0) });
        break;
      case "withdraw":
        if (msg.amount > log.reduce(reduceLog, 0)) {
          socket.end({
            cmd: "error",
            msg: "Bank does not have that much"
          });
        } else {
          msg.amount *= -1;
          appendToTransactionLog(msg);
          socket.end({
            cmd: "balance",
            balance: log.reduce(reduceLog, 0)
          });
        }
        break;
      default:
        socket.end({ cmd: "error", msg: "Unknown command" });
        break;
    }
  });
});

server.listen(3876);

const reduceLog = (balance, log) => {
  return balance + log.value.amount;
};

const hashToHex = entry => {
  return web3u.sha3(entry);
};

// Append a new transaction with its hash and value
const appendToTransactionLog = entry => {
  const genesisHash = Buffer.alloc(32).toString("hex");
  const previousHash = log.length ? log[log.length - 1].hash : genesisHash;
  const hash = hashToHex(previousHash + JSON.stringify(entry));
  log.push({
    value: entry,
    hash: hash
  });

  fs.writeFile("./log.json", JSON.stringify(log, null, 2), function(err) {
    if (err) throw err;
  });

  return hash;
};

const verifyChainIntegrity = log => {
  log.map((current, index, array) => {
    let prevHash = index
      ? array[index - 1].hash
      : Buffer.alloc(32).toString("hex");
    let hash = hashToHex(prevHash + JSON.stringify(current.value));
    if (hash != current.hash) {
      console.log("Hash verification problem!");
      process.exit(1);
    }
    return true;
  });
};
