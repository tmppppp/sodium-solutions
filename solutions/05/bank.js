var jsonStream = require("duplex-json-stream");
var net = require("net");
const fs = require("fs");
const { sha256 } = require("ethereumjs-util");
const crypto = require("crypto");

fs.writeFile("./log.json", "[]", { flag: "wx" }, function(err) {});

var server = net.createServer(function(socket) {
  socket = jsonStream(socket);

  socket.on("data", function(msg) {
    let log = JSON.parse(fs.readFileSync("./log.json", "utf8"));
    hash = log.hash;
    log = log.log;

    if (sha256("salt" + JSON.stringify(log, null, 2)).toString("hex") != hash) {
      console.log("LOG CORRUPTED");
      process.exit();
    }
    console.log("Bank received:", msg);

    switch (msg.cmd) {
      case "balance":
        socket.end({ cmd: "balance", balance: log.reduce(reduceLog, 0) });
        break;
      case "deposit":
        log.push(msg);
        socket.end({ cmd: "balance", balance: log.reduce(reduceLog, 0) });
        break;
      case "withdraw":
        if (msg.amount > log.reduce(reduceLog, 0)) {
          socket.end({
            cmd: "error",
            msg: "Bank does not have that much"
          });
        } else {
          msg.amount *= -1;
          log.push(msg);
          socket.end({
            cmd: "balance",
            balance: log.reduce(reduceLog, 0)
          });
        }
        break;
      default:
        socket.end({ cmd: "error", msg: "Unknown command" });
        break;
    }

    let newLog = {};
    newLog.log = log;
    newLog.hash = sha256("salt" + JSON.stringify(log, null, 2)).toString("hex");
    fs.writeFile("./log.json", JSON.stringify(newLog, null, 2), function(err) {
      if (err) throw err;
    });
  });
});

server.listen(3876);

function reduceLog(balance, entry) {
  return balance + entry.amount;
}
