var jsonStream = require("duplex-json-stream");
var net = require("net");
const fs = require("fs");
const { sha256 } = require("ethereumjs-util");
const crypto = require("crypto");
const web3u = require("web3-utils");

const EthCrypto = require("eth-crypto");

const keythereum = require("keythereum");

var CryptoJS = require("crypto-js");
var EC = require("elliptic").ec;
var ec = new EC("secp256k1");

var keyPair = {};
if (fs.existsSync("./keypair.json")) {
  keyPair = require("./keypair.json");
} else {
  private = computePrivKey();
  public = computeAddressFromPrivKey(private);
  keyPair.private = "0x" + private;
  keyPair.public = "0x" + public;

  fs.writeFile("./keypair.json", JSON.stringify(keyPair, null, 2), function(
    err
  ) {
    if (err) throw err;
  });
}

function computeAddressFromPrivKey(privKey) {
  var keyPair = ec.genKeyPair();
  keyPair._importPrivate(privKey, "hex");
  var compact = false;
  var pubKey = keyPair.getPublic(compact, "hex").slice(2);
  var pubKeyWordArray = CryptoJS.enc.Hex.parse(pubKey);
  var hash = CryptoJS.SHA3(pubKeyWordArray, { outputLength: 256 });
  var address = hash.toString(CryptoJS.enc.Hex).slice(24);

  return address;
}

function computePrivKey() {
  var params = { keyBytes: 32, ivBytes: 16 };

  var dk = keythereum.create(params);
  //console.log(dk);

  let private = dk.privateKey.toString("hex");
  return private;
}

var log = [];
fs.writeFile("./log.json", "[]", { flag: "wx" }, function(err) {});

var server = net.createServer(function(socket) {
  socket = jsonStream(socket);

  socket.on("data", function(msg) {
    console.log("Bank received:", msg);
    log = JSON.parse(fs.readFileSync("./log.json", "utf8"));
    verifyChainIntegrity(log);
    switch (msg.cmd) {
      case "balance":
        socket.end({ cmd: "balance", balance: log.reduce(reduceLog, 0) });
        break;
      case "deposit":
        appendToTransactionLog(msg);
        socket.end({ cmd: "balance", balance: log.reduce(reduceLog, 0) });
        break;
      case "withdraw":
        if (msg.amount > log.reduce(reduceLog, 0)) {
          socket.end({
            cmd: "error",
            msg: "Bank does not have that much"
          });
        } else {
          msg.amount *= -1;
          appendToTransactionLog(msg);
          socket.end({
            cmd: "balance",
            balance: log.reduce(reduceLog, 0)
          });
        }
        break;
      default:
        socket.end({ cmd: "error", msg: "Unknown command" });
        break;
    }
  });
});

server.listen(3876);

const reduceLog = (balance, log) => {
  //console.log(log);
  return balance + log.tx.value.amount;
};

const hashToHex = entry => {
  return web3u.sha3(entry);
};

// Append a new transaction with its hash and value
const appendToTransactionLog = entry => {
  const genesisHash = Buffer.alloc(32).toString("hex");
  const previousHash = log.length ? log[log.length - 1].hash : genesisHash;
  const hash = hashToHex(previousHash + JSON.stringify(entry));
  let tx = {
    value: entry,
    hash: hash
  };

  let log_entry = {
    tx: tx,
    signature: EthCrypto.sign(
      keyPair.private,
      hashToHex(JSON.stringify(tx)) // hash of message
    )
  };

  log.push(log_entry);

  fs.writeFile("./log.json", JSON.stringify(log, null, 2), function(err) {
    if (err) throw err;
  });

  return hash;
};

const verifyChainIntegrity = log => {
  log.map((current, index, array) => {
    tx = current.tx;
    signature = current.signature;
    let prevHash = index
      ? array[index - 1].hash
      : Buffer.alloc(32).toString("hex");
    let hash = hashToHex(prevHash + JSON.stringify(tx.value));
    if (hash != tx.hash) {
      console.log("Hash verification problem!");
      process.exit(1);
    }

    signer = EthCrypto.recover(
      signature,
      hashToHex(JSON.stringify(tx)) // signed message hash
    ).toLowerCase();

    if (signer != keyPair.public) {
      console.log(`signer ${signer} != ${keyPair.public} keyPair.public!`);
      process.exit(1);
    }

    return true;
  });
};
