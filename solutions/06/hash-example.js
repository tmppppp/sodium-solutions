var jsonStream = require("duplex-json-stream");
var net = require("net");
const fs = require("fs");
const { sha256 } = require("ethereumjs-util");
const crypto = require("crypto");

var sodium = require("sodium-native");
var output = Buffer.alloc(sodium.crypto_generichash_BYTES);
var input = Buffer.from("Hello, World!");

// Compute blake2b hash
sodium.crypto_generichash(output, input);

// Convert bytes to printable string
console.log(output.toString("hex"));
