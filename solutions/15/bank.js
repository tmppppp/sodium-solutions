var jsonStream = require("duplex-json-stream");
var net = require("net");
const fs = require("fs");
const { sha256 } = require("ethereumjs-util");
const crypto = require("crypto");
const web3u = require("web3-utils");

var sodium = require("sodium-native");

const EthCrypto = require("eth-crypto");

const keythereum = require("keythereum");

var CryptoJS = require("crypto-js");
var EC = require("elliptic").ec;
var ec = new EC("secp256k1");

var keyPair = {};
if (fs.existsSync("./keypair.json")) {
  keyPair = require("./keypair.json");
} else {
  private = computePrivKey();
  public = computeAddressFromPrivKey(private);
  keyPair.private = "0x" + private;
  keyPair.public = "0x" + public;

  fs.writeFile("./keypair.json", JSON.stringify(keyPair, null, 2), function(
    err
  ) {
    if (err) throw err;
  });
}

var symmetricKey;
var symmetricKeyBuffer;

if (fs.existsSync("./symkey.json")) {
  symmetricKey = require("./symkey.json").key;
  symmetricKeyBuffer = Buffer.from(symmetricKey, "hex");
  sodiumSymmetricKeyBuffer = sodium.sodium_malloc(
    symmetricKeyBuffer.byteLength
  );
  symmetricKeyBuffer.copy(sodiumSymmetricKeyBuffer);
  symmetricKeyBuffer = sodiumSymmetricKeyBuffer;
} else {
  symmetricKeyBuffer = sodium.sodium_malloc(sodium.crypto_secretbox_KEYBYTES);
  sodium.randombytes_buf(symmetricKeyBuffer);
  hexKey = symmetricKeyBuffer.toString("hex");

  fs.writeFile(
    "./symkey.json",
    JSON.stringify({ key: hexKey }, null, 2),
    function(err) {
      if (err) throw err;
    }
  );
}

sodium.sodium_mprotect_noaccess(symmetricKeyBuffer);

function computeAddressFromPrivKey(privKey) {
  var keyPair = ec.genKeyPair();
  keyPair._importPrivate(privKey, "hex");
  var compact = false;
  var pubKey = keyPair.getPublic(compact, "hex").slice(2);
  var pubKeyWordArray = CryptoJS.enc.Hex.parse(pubKey);
  var hash = CryptoJS.SHA3(pubKeyWordArray, { outputLength: 256 });
  var address = hash.toString(CryptoJS.enc.Hex).slice(24);

  return address;
}

function computePrivKey() {
  var params = { keyBytes: 32, ivBytes: 16 };

  var dk = keythereum.create(params);
  //console.log(dk);

  let private = dk.privateKey.toString("hex");
  return private;
}

const encrypt = something => {
  var nonceBuffer = Buffer.alloc(sodium.crypto_secretbox_NONCEBYTES);
  sodium.randombytes_buf(nonceBuffer);
  messageBuffer = Buffer.from(JSON.stringify(something));

  var cipherBuffer = Buffer.alloc(
    messageBuffer.length + sodium.crypto_secretbox_MACBYTES
  );
  sodium.sodium_mprotect_readwrite(symmetricKeyBuffer);
  sodium.crypto_secretbox_easy(
    cipherBuffer,
    messageBuffer,
    nonceBuffer,
    symmetricKeyBuffer
  );
  sodium.sodium_mprotect_noaccess(symmetricKeyBuffer);

  out_json = {
    nonce: nonceBuffer.toString("hex"),
    ciphertext: cipherBuffer.toString("hex")
  };

  return out_json;
};

const decrypt = something => {
  ciphertext = something.ciphertext;
  nonce = something.nonce;

  cipherBuffer = Buffer.from(ciphertext, "hex");
  nonceBuffer = Buffer.from(nonce, "hex");

  var plainText = Buffer.alloc(
    cipherBuffer.length - sodium.crypto_secretbox_MACBYTES
  );
  sodium.sodium_mprotect_readwrite(symmetricKeyBuffer);
  sodium.crypto_secretbox_open_easy(
    plainText,
    cipherBuffer,
    nonceBuffer,
    symmetricKeyBuffer
  );
  sodium.sodium_mprotect_noaccess(symmetricKeyBuffer);

  return JSON.parse(plainText.toString());
};

var logMap = {};

var server = net.createServer(function(socket) {
  socket = jsonStream(socket);

  socket.on("data", function(msg) {
    console.log("Bank received:", msg);

    if (fs.existsSync("./log.json")) {
      var encrLogInJson = JSON.parse(fs.readFileSync("./log.json", "utf8"));
      logMap = decrypt(encrLogInJson);
    } else {
      logMap = {};
    }

    var customerEntry;
    if ("tx" in msg && "keyPair" in msg.tx && "public" in msg.tx.keyPair) {
      customerEntry = logMap[msg.tx.keyPair.public];
    }

    if (!customerEntry) {
      if (msg.cmd == "register") {
        customerEntry = {};
        customerEntry.keyPair = msg.keyPair;
        customerEntry.customerLog = [];
        logMap[msg.keyPair.public] = customerEntry;
        writeLogMapToFile();
      } else {
        socket.end({
          cmd: "error",
          msg: `customerId ${msg.keyPair.public} does not exist`
        });
      }
      return;
    }

    customerKeyPair = customerEntry.keyPair;

    signature = msg.signature;
    msg = msg.tx; // unwrap

    signer = EthCrypto.recover(
      signature,
      hashToHex(JSON.stringify(msg)) // signed message hash
    ).toLowerCase();

    if (signer != customerKeyPair.public) {
      socket.end({
        cmd: "error",
        msg: `signer ${signer} != ${customerKeyPair.public} keyPair.public!`
      });
      return;
    }

    customerLog = customerEntry.customerLog;
    verifyChainIntegrity(customerLog);

    switch (msg.cmd) {
      case "register":
        socket.end({
          cmd: "error",
          msg: `customerId ${msg.customerId} cannot be registered again`
        });
        return;
      case "balance":
        socket.end({
          cmd: "balance",
          balance: customerLog.reduce(reduceLog, 0)
        });
        break;
      case "deposit":
        appendToTransactionLog(msg);
        socket.end({
          cmd: "balance",
          balance: customerLog.reduce(reduceLog, 0)
        });
        break;
      case "withdraw":
        if (msg.amount > customerLog.reduce(reduceLog, 0)) {
          socket.end({
            cmd: "error",
            msg: "Bank does not have that much"
          });
        } else {
          msg.amount *= -1;
          appendToTransactionLog(msg);
          socket.end({
            cmd: "balance",
            balance: customerLog.reduce(reduceLog, 0)
          });
        }
        break;
      default:
        socket.end({ cmd: "error", msg: "Unknown command" });
        break;
    }
  });
});

server.listen(3876);

const reduceLog = (balance, log) => {
  return balance + log.tx.value.amount;
};

const hashToHex = entry => {
  return web3u.sha3(entry);
};

// Append a new transaction with its hash and value
const appendToTransactionLog = entry => {
  const genesisHash = Buffer.alloc(32).toString("hex");
  const previousHash = customerLog.length
    ? customerLog[customerLog.length - 1].hash
    : genesisHash;
  const hash = hashToHex(previousHash + JSON.stringify(entry));
  let tx = {
    value: entry,
    hash: hash
  };

  let log_entry = {
    tx: tx,
    signature: EthCrypto.sign(
      keyPair.private,
      hashToHex(JSON.stringify(tx)) // hash of message
    )
  };

  customerLog.push(log_entry);

  writeLogMapToFile();

  return hash;
};

const verifyChainIntegrity = log => {
  log.map((current, index, array) => {
    tx = current.tx;
    signature = current.signature;
    let prevHash = index
      ? array[index - 1].hash
      : Buffer.alloc(32).toString("hex");
    let hash = hashToHex(prevHash + JSON.stringify(tx.value));
    if (hash != tx.hash) {
      console.log("Hash verification problem!");
      process.exit(1);
    }

    signer = EthCrypto.recover(
      signature,
      hashToHex(JSON.stringify(tx)) // signed message hash
    ).toLowerCase();

    if (signer != keyPair.public) {
      console.log(`signer ${signer} != ${keyPair.public} keyPair.public!`);
      process.exit(1);
    }

    return true;
  });
};
function writeLogMapToFile() {
  ciphertext_json = encrypt(logMap);
  fs.writeFile("./log.json", JSON.stringify(ciphertext_json, null, 2), function(
    err
  ) {
    if (err) throw err;
  });
}
