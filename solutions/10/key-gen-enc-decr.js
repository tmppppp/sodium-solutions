var sodium = require("sodium-native");

message = "Hello, World!";

console.log(`Initial = ${message}`);

var key = Buffer.alloc(sodium.crypto_secretbox_KEYBYTES);
sodium.randombytes_buf(key);
console.log(`key = ${key.toString("hex")}`);

////

var message = Buffer.from(message);

var nonce = Buffer.alloc(sodium.crypto_secretbox_NONCEBYTES);
sodium.randombytes_buf(nonce);

var cipher = Buffer.alloc(message.length + sodium.crypto_secretbox_MACBYTES);
sodium.crypto_secretbox_easy(cipher, message, nonce, key);

console.log(
  `Encrypted: ${cipher.toString("hex")} nonce = ${nonce.toString("hex")}`
);

////
var plainText = Buffer.alloc(cipher.length - sodium.crypto_secretbox_MACBYTES);

sodium.crypto_secretbox_open_easy(plainText, cipher, nonce, key);

console.log("Plaintext:", plainText.toString());
