var jsonStream = require("duplex-json-stream");
var net = require("net");
const fs = require("fs");

fs.writeFile("./log.json", "[]", { flag: "wx" }, function(err) {});

var server = net.createServer(function(socket) {
  socket = jsonStream(socket);

  socket.on("data", function(msg) {
    var log = require("./log.json");
    console.log("Bank received:", msg);

    switch (msg.cmd) {
      case "balance":
        socket.end({ cmd: "balance", balance: log.reduce(reduceLog, 0) });
        break;
      case "deposit":
        log.push(msg);
        socket.end({ cmd: "balance", balance: log.reduce(reduceLog, 0) });
        break;
      case "withdraw":
        if (msg.amount > log.reduce(reduceLog, 0)) {
          socket.end({
            cmd: "error",
            msg: "Bank does not have that much"
          });
        } else {
          msg.amount *= -1;
          log.push(msg);
          socket.end({
            cmd: "balance",
            balance: log.reduce(reduceLog, 0)
          });
        }
        break;
      default:
        socket.end({ cmd: "error", msg: "Unknown command" });
        break;
    }

    fs.writeFile("./log.json", JSON.stringify(log, null, 2), function(err) {
      if (err) throw err;
    });
  });
});

server.listen(3876);

function reduceLog(balance, entry) {
  return balance + entry.amount;
}
