//https://kobl.one/blog/create-full-ethereum-keypair-and-address/

// openssl ecparam -name secp256k1 -genkey => EC pub/priv
// eth_pub_key = keccak-256_hash(pub).TAKE_LAST_20_bytes

var jsonStream = require("duplex-json-stream");
var net = require("net");
const fs = require("fs");
const crypto = require("crypto");

const util = require("ethereumjs-util");
const EthCrypto = require("eth-crypto");

const keythereum = require("keythereum");

var CryptoJS = require("crypto-js");
var EC = require("elliptic").ec;
var ec = new EC("secp256k1");

function computeAddressFromPrivKey(privKey) {
  var keyPair = ec.genKeyPair();
  keyPair._importPrivate(privKey, "hex");
  var compact = false;
  var pubKey = keyPair.getPublic(compact, "hex").slice(2);
  var pubKeyWordArray = CryptoJS.enc.Hex.parse(pubKey);
  var hash = CryptoJS.SHA3(pubKeyWordArray, { outputLength: 256 });
  var address = hash.toString(CryptoJS.enc.Hex).slice(24);

  return address;
}

function computePrivKey() {
  var params = { keyBytes: 32, ivBytes: 16 };

  var dk = keythereum.create(params);
  //console.log(dk);

  let private = dk.privateKey.toString("hex");
  return private;
}

private = computePrivKey();
public = computeAddressFromPrivKey(private);

private0x = "0x" + private;
public0x = "0x" + public;

console.log("Pub " + private0x);
console.log("Priv " + public0x + "\n\n");

const message = "foobar";
const messageHash = EthCrypto.hash.keccak256(message);

const signature = EthCrypto.sign(
  private0x,
  messageHash // hash of message
);
//sign.js should generate a
// key - pair

console.log(
  `message = ${message}\n messageHash = ${messageHash}\n publicKey = ${public0x}\n sig = ${signature}\n\n`
);

const signer = EthCrypto.recover(
  signature,
  messageHash // signed message hash
).toLowerCase;

console.log(
  `inputPublicKey = ${public0x} \n message = ${message}\n messageHash = ${messageHash}\n outputPublicKey = ${signer} pb_input == pb_output => ${public0x ===
    signer}\n\n`
);
